//
//  Extentions.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 02.12.2021.
//

import Foundation

extension Date {
    var toString: String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter.string(from: self)
    }
}

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
}
