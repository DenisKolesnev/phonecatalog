//
//  UserDef.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 01.12.2021.
//

import Foundation

struct UserDef {

    static private let userDefaults = UserDefaults.standard
    static private let firstLaunchKey = "FirstLaunch"

    static var isNotFirstLaunch: Bool {
        get { return userDefaults.bool(forKey: firstLaunchKey) }
        set { userDefaults.set(newValue, forKey: firstLaunchKey) }
    }

}
