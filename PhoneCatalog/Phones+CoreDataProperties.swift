//
//  Phones+CoreDataProperties.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 01.12.2021.
//
//

import Foundation
import CoreData


extension Phones {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Phones> {
        return NSFetchRequest<Phones>(entityName: "Phones")
    }

    @NSManaged public var title: String
    @NSManaged public var model: String
    @NSManaged public var makeDate: Date
    @NSManaged public var systemVersion: String
    @NSManaged public var maker: String
    @NSManaged public var photo: Data?

}

extension Phones : Identifiable {

}
