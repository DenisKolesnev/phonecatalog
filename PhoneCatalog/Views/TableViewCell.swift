//
//  TableViewCell.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 01.12.2021.
//

import UIKit

class TableViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelModel: UILabel!

    // MARK: - Public Properties
    var phone: Phones?

    // MARK: - Override Methods
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        labelTitle.text = phone?.title
        labelModel.text = phone?.model

        if let photoData = phone?.photo {
            photo.image = UIImage(data: photoData)
        } else {
            photo.image = UIImage(named: "noImage")
        }
    }

}
