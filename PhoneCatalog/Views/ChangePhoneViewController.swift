//
//  ChangePhoneViewController.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 02.12.2021.
//

import UIKit
import AVFoundation

class ChangePhoneViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAdaptivePresentationControllerDelegate {

    // MARK: - Outlets
    @IBOutlet weak var buttonDelete: UIButton!
    @IBOutlet weak var textFieldTitle: UITextField!
    @IBOutlet weak var textFieldModel: UITextField!
    @IBOutlet weak var textFieldMaker: UITextField!
    @IBOutlet weak var textFieldSystemVersion: UITextField!
    @IBOutlet weak var datePickerMakeData: UIDatePicker!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var buttonDeletePhoto: UIButton!

    // MARK: - Public Properties
    var phone: Phones?
    weak var delegate: UpdatePhonesViewControllerDelegate?

    // MARK: - Private Properties
    private let imagePicker = UIImagePickerController()
    private var noPhoto = true {
        didSet { enabledDeletePhotoButton() }
    }

    private var isAllFieldsFill: Bool {
        guard !textFieldTitle.text!.isEmpty else { return false }
        guard !textFieldModel.text!.isEmpty else { return false }
        guard !textFieldMaker.text!.isEmpty else { return false }
        guard !textFieldSystemVersion.text!.isEmpty else { return false }
        return true
    }

    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        presentationController?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification, object: nil)

        buttonDelete.isHidden = (phone == nil)

        textFieldTitle.text = phone?.title
        textFieldModel.text = phone?.model
        textFieldMaker.text = phone?.maker
        textFieldSystemVersion.text = phone?.systemVersion
        datePickerMakeData.date = phone?.makeDate ?? Date()

        if let photoData = phone?.photo {
            imageViewPhoto.image = UIImage(data: photoData)
            noPhoto = false
        } else {
            imageViewPhoto.image = UIImage(named: "noImage")
        }

        isModalInPresentation = (phone != nil)
        enabledDeletePhotoButton()
    }

    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }

    // MARK: - Keyboard
    @objc func keyboardWillShow(notification: NSNotification) {
        let bottomHeight = view.bounds.height - datePickerMakeData.frame.origin.y - datePickerMakeData.frame.height - 10
        if let keyboardHeight = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height {
            if keyboardHeight > bottomHeight {
                self.view.window?.frame.origin.y = bottomHeight - keyboardHeight
            }
        }
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        self.view.window?.frame.origin.y = 0
    }

    // MARK: - PresentationController
    func presentationControllerDidAttemptToDismiss(_ presentationController: UIPresentationController) {
        let alert = UIAlertController(title: "Select Action".localized, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Save".localized, style: .default) { _ in self.saveAndDismiss() })
        alert.addAction(UIAlertAction(title: "Not Save".localized, style: .destructive) { _ in self.dismiss(animated: true) })
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel))
        present(alert, animated: true)
    }

    // MARK: - Private Methods
    private func enabledDeletePhotoButton() {
        buttonDeletePhoto.isEnabled = !noPhoto
    }

    private func selectPhoto(_ sourceType: UIImagePickerController.SourceType) {
        imagePicker.sourceType = sourceType

        if sourceType == .camera {
            imagePicker.cameraCaptureMode = .photo
            AVCaptureDevice.requestAccess(for: .video) { authorized in
                DispatchQueue.main.async {
                    if authorized {
                        self.present(self.imagePicker, animated: true, completion: nil)
                    } else {
                        self.showCameraAlert()
                    }
                }
            }
        }

        if sourceType == .photoLibrary {
            present(imagePicker, animated: true, completion: nil)
        }
    }

    func showCameraAlert() {
        let alert = UIAlertController(title: "Unable to access the camera".localized,
                                      message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app".localized,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Settings".localized, style: .default) { _ in
            guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else { return }
            if UIApplication.shared.canOpenURL(settingsUrl) {
                UIApplication.shared.open(settingsUrl) { _ in }
            }
        })
        present(alert, animated: true, completion: nil)
    }

    private func showNotAllFieldsFillAlert() {
        let alert = UIAlertController(title: "Attention!".localized,
                                      message: "Not all fields are filled".localized,
                                      preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }

    private func showDeletePhoneAlert(handler: ((UIAlertAction) -> Void)?) {
        let alert = UIAlertController(title: "Do you really want to delete this record?".localized,
                                      message: nil,
                                      preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Delete".localized, style: .destructive, handler: handler))
        alert.addAction(UIAlertAction(title: "Cancel".localized, style: .cancel))
        present(alert, animated: true)
    }

    private func saveAndDismiss() {
        if !isAllFieldsFill {
            showNotAllFieldsFillAlert()
            return
        }

        if phone == nil {
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                phone = Phones(context: appDelegate.persistentContainer.viewContext)
            } else {
                dismiss(animated: true, completion: nil)
            }
        }

        phone?.title = textFieldTitle.text ?? ""
        phone?.model = textFieldModel.text ?? ""
        phone?.maker = textFieldMaker.text ?? ""
        phone?.systemVersion = textFieldSystemVersion.text ?? ""
        phone?.makeDate = datePickerMakeData.date
        phone?.photo = noPhoto ? nil : imageViewPhoto.image?.jpegData(compressionQuality: 1)
        phone?.save()
        delegate?.updatePhones()
        dismiss(animated: true, completion: nil)
    }

    // MARK: - ImagePicker Methods
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.originalImage] as? UIImage
        self.imageViewPhoto.image = image
        noPhoto = (image == nil)
        imagePicker.dismiss(animated: true, completion: nil)
     }

    // MARK: - Actions
    @IBAction func deleteButtonTouch(_ sender: UIButton) {
        showDeletePhoneAlert { _ in
            let detalViewController = self.delegate as? DetalViewController
            self.phone?.delete()
            detalViewController?.delegate?.updatePhones()
            detalViewController?.navigationController?.popToRootViewController(animated: false)
            self.dismiss(animated: true, completion: nil)
        }
    }

    @IBAction func selectPhotoButtonTouch(_ sender: UIButton) {
        selectPhoto(.photoLibrary)
    }

    @IBAction func takePhotoButtonTouch(_ sender: UIButton) {
        selectPhoto(.camera)
    }

    @IBAction func deletePhotoButtonTouch(_ sender: UIButton) {
        imageViewPhoto.image = UIImage(named: "noImage")
        noPhoto = true
    }

    @IBAction func textFieldDone(_ sender: UITextField) {
        sender.resignFirstResponder()
    }

    @IBAction func cancelButtonTouch(_ sender: UIBarButtonItem) {
        dismiss(animated: true)
    }

    @IBAction func doneButtonTouch(_ sender: UIBarButtonItem) {
        saveAndDismiss()
    }

    @IBAction func textFieldEditing(_ sender: UITextField) {
        isModalInPresentation = !sender.text!.isEmpty
    }

}
