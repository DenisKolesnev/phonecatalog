//
//  ViewController.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 01.12.2021.
//

import UIKit

protocol UpdatePhonesViewControllerDelegate: AnyObject {
    func updatePhones()
}

class CatalogViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchResultsUpdating, UpdatePhonesViewControllerDelegate {

    // MARK: - Outlets
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Private Properties
    private var allPhones = [Phones]()
    private var filteredPhones = [Phones]()
    private let searchController = UISearchController(searchResultsController: nil)


    // MARK: - Private Methods
    private func insertSearchController() {
        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search".localized
        navigationItem.searchController = searchController
    }

    // MARK: Public Methods
    func updatePhones() {
        allPhones = Phones.allRecords.sorted { $0.title < $1.title }
        updateSearchResults(for: searchController)
    }

    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        insertSearchController()

        if !UserDef.isNotFirstLaunch {
            setDefaultData()
            UserDef.isNotFirstLaunch = true
        }
        updatePhones()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowDetalSegue" {
            guard let destination = segue.destination as? DetalViewController else { return }
            guard let cell = sender as? TableViewCell else { return }
            destination.phone = cell.phone
            destination.delegate = self
        }

        if segue.identifier == "InsertPhoneSegue" {
            guard let destination = segue.destination as? ChangePhoneViewController else { return }
            destination.phone = nil
            destination.delegate = self
        }
    }

    // MARK: - TableView Methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredPhones.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "TableCell") as? TableViewCell else {
            return UITableViewCell()
        }
        cell.phone = filteredPhones[indexPath.row]
        return cell
    }

    // MARK: - SearchController
    func updateSearchResults(for searchController: UISearchController) {
        guard let searchText = searchController.searchBar.text?.lowercased() else { return }
        if searchText.isEmpty {
            filteredPhones = allPhones
        } else {
            filteredPhones = allPhones.filter {
                $0.title.lowercased().contains(searchText) || $0.model.lowercased().contains(searchText)
            }
        }
        tableView.reloadData()
    }
}

