//
//  DetalViewController.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 01.12.2021.
//

import UIKit

class DetalViewController: UIViewController, UpdatePhonesViewControllerDelegate {

    // MARK: - Outlets
    @IBOutlet weak var labelModel: UILabel!
    @IBOutlet weak var labelMaker: UILabel!
    @IBOutlet weak var labelSystemVersion: UILabel!
    @IBOutlet weak var labelMakeData: UILabel!
    @IBOutlet weak var imageViewPhoto: UIImageView!

    // MARK: - Public Properties
    var phone: Phones?
    weak var delegate: UpdatePhonesViewControllerDelegate?

    // MARK: - Public Methods
    func updatePhones() {
        if phone == nil {
            dismiss(animated: true, completion: nil)
            return
        }

        delegate?.updatePhones()

        title = phone?.title
        labelModel.text = phone?.model
        labelMaker.text = phone?.maker
        labelSystemVersion.text = phone?.systemVersion
        labelMakeData.text = phone?.makeDate.toString
        if let photo = phone?.photo {
            imageViewPhoto.image = UIImage(data: photo)
        } else {
            imageViewPhoto.image = UIImage(named: "noImage")
        }
    }

    // MARK: - Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        updatePhones()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "ChangePhoneSeque" else { return }
        guard let destination = segue.destination as? ChangePhoneViewController else { return }
        destination.phone = self.phone
        destination.delegate = self
    }

}
