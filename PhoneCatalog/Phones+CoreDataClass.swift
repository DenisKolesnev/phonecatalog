//
//  Phones+CoreDataClass.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 01.12.2021.
//
//

import Foundation
import CoreData
import UIKit

@objc(Phones)
public class Phones: NSManagedObject {

    static private let appDelegate = UIApplication.shared.delegate as? AppDelegate

    static var allRecords: [Phones] {
        guard let appDelegate = self.appDelegate else { return [] }
        do {
            let fetchRequest = Phones.fetchRequest()
            return try appDelegate.persistentContainer.viewContext.fetch(fetchRequest)
        } catch {
            return []
        }
    }

    func save() {
        Phones.appDelegate?.saveContext()
    }

    func delete() {
        Phones.appDelegate?.persistentContainer.viewContext.delete(self)
        save()
    }

}
