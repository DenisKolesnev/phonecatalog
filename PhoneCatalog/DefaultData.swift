//
//  DefaultData.swift
//  PhoneCatalog
//
//  Created by Denis Kolesnev on 01.12.2021.
//

import Foundation
import UIKit

fileprivate func getPhone(title: String, model: String, maker: String, version: String) -> Phones? {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return nil }
    let phone = Phones(context: appDelegate.persistentContainer.viewContext)
    phone.title = title
    phone.model = model
    phone.maker = maker
    phone.makeDate = Date()
    phone.systemVersion = version
    return phone
}

func setDefaultData() {
    getPhone(title: "Телефон 1", model: "iPhone 6s", maker: "Apple", version: "iOS 13")?.save()
    getPhone(title: "Телефон 2", model: "iPhone 11 Pro Max", maker: "Apple", version: "iOS 15")?.save()
    getPhone(title: "Телефон 3", model: "Galaxy", maker: "Samsung", version: "Android")?.save()
    getPhone(title: "Телефон 4", model: "Honor", maker: "Huawey", version: "Android")?.save()
    getPhone(title: "Телефон 5", model: "Mi 11 Late", maker: "Xiaomi", version: "Android")?.save()
}
